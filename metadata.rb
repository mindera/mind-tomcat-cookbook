name             'mind-tomcat'
maintainer       'Mindera'
maintainer_email 'miguel.fonseca@mindera.com'
license          'Apache 2.0'
description      'Installs/Configures Tomcat'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.0.1'

depends 'tomcat', '~> 3.2.2'
depends 'openssl', '2.0.2'
depends 'java', '1.50.0'

supports 'centos', '~> 6.0'
supports 'amazon'
