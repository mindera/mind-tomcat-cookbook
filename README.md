# Mindera Tomcat Cookbook

Mindera wrapper cookbook to install [Tomcat](http://tomcat.apache.org/) using the community cookbook [opscode-cookbooks/tomcat](https://github.com/opscode-cookbooks/tomcat).

## Requirements

Depends on the cookbooks:

 * tomcat

## Platforms

 * Centos 6+
 * Amazon Linux - (WIP)

## Attributes

TODO

## Recipes

### default

Installs tomcat with a minimal configuration. It defaults to tomcat 7 and openjdk 1.7.

## Development / Contributing

### Tests

This assumes you're using [chef-dk](https://downloads.getchef.com/chef-dk/).

To run unit tests using chefspec you can run:
```bash
$ chef exec rspec
```

To run integration tests using kitchen-ci and serverspec you can run:
```bash
$ chef exec kitchen test
```

