require 'serverspec'

# Required by serverspec
set :backend, :exec

RSpec.configure do |c|
  c.before :all do
    c.path = '/sbin:/usr/sbin:$PATH'
  end
end

describe 'tomcat' do
  it 'should be enabled' do
    expect(service('tomcat')).to be_enabled
  end

  it 'should be running' do
    expect(service('tomcat')).to be_running
  end

  it 'should be listening on tcp port 8080' do
    expect(port('8080')).to be_listening.with('tcp')
  end
end
