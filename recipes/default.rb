#
# Cookbook Name:: mind-tomcat
# Recipe:: default
#
# Copyright 2014, Mindera
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Mind-Tomcat
actions = node['mind-tomcat']['service']['actions']
restart_action = node['mind-tomcat']['service']['restart']

# Configure java
include_recipe 'java'

# Install tomcat
tomcat_install 'tomcat' do
  version "#{node['tomcat']['version']}"
  tomcat_user 'tomcat'
  tomcat_group 'tomcat'
end

# Define tomcat service
tomcat_service 'tomcat' do
  action actions
  tomcat_user 'tomcat'
  tomcat_group 'tomcat'
  env_vars [ {'JAVA_OPTS' => node['tomcat']['java_options']}, {'CATALINA_PID' => '$CATALINA_BASE/tomcat.pid'} ]
end

# Deploy tomcat server xml
template "#{node['tomcat']['base_dir']}/conf/server.xml" do
  source 'server.xml.erb'
  owner 'tomcat'
  group 'tomcat'
  mode '0664'
  backup false
  notifies :restart, 'tomcat_service[tomcat]', :delayed if restart_action
end
