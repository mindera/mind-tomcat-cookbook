require 'spec_helper'

describe 'mind-tomcat::default' do
  cached(:chef_run) { ChefSpec::Runner.new.converge(described_recipe) }
  cached(:node) { chef_run.node }
  cached(:config) do
    chef_run.template("#{node['tomcat']['config_dir']}/tomcat.conf")
  end
  cached(:server_xml) do
    chef_run.template("#{node['tomcat']['config_dir']}/server.xml")
  end
  cached(:init) do
    chef_run.template("#{node['tomcat']['init_dir']}/tomcat")
  end

  it 'include the tomcat recipe' do
    expect(chef_run).to include_recipe('tomcat')
  end

  it 'define and enable and start the tomcat service' do
    expect(chef_run).to enable_service('tomcat')
    expect(chef_run).to start_service('tomcat')
  end

  it 'deploy general tomcat config and notifies service "tomcat"' do
    expect(chef_run).to create_template("#{node['tomcat']['config_dir']}/tomcat.conf") \
    .with(
      source: 'tomcat.conf.erb',
      mode: '0664',
      owner: 'tomcat',
      group: 'tomcat'
    )
    expect(config).to notify("service[tomcat]").to(:restart).delayed
  end

  it 'deploy tomcat server xml and notifies service "tomcat"' do
    expect(chef_run).to create_template("#{node['tomcat']['config_dir']}/server.xml") \
    .with(
      source: 'server.xml.erb',
      mode: '0664',
      owner: 'tomcat',
      group: 'tomcat'
    )
    expect(server_xml).to notify("service[tomcat]").to(:restart).delayed
  end

  it 'deploy tomcat init script and notifies service "tomcat"' do
    expect(chef_run).to create_template("#{node['tomcat']['init_dir']}/tomcat") \
    .with(
      source: 'tomcat.initd.xml.erb',
      mode: '0755',
      owner: 'root',
      group: 'root'
    )
    expect(init).to notify("service[tomcat]").to(:restart).delayed
  end

end
