#
# Cookbook Name:: mind-tomcat
# Attributes:: default
#
# Copyright 2014, Mindera
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Tomcat attributes
#  - cookbook: https://supermarket.getchef.com/cookbooks/tomcat

default['tomcat']['version'] = '9.0.22'
#default['tomcat']['base_version'] = '_tomcat_9_0_22'
default['tomcat']['base_dir'] = "/opt/tomcat_tomcat"
default['tomcat']['config_dir'] = "#{node["tomcat"]["base_dir"]}/conf"
default['tomcat']['webapp_dir'] = "#{node["tomcat"]["base_dir"]}/webapps"
default['tomcat']['port'] = 8090

# Java attributes
# - cookbook: https://supermarket.getchef.com/cookbooks/java

default['java']['jdk_version'] = 8

# Mind-Tomcat attributes

default['mind-tomcat']['service']['actions'] = [:enable, :start]
default['mind-tomcat']['service']['restart'] = true
